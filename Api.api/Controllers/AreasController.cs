﻿using Api.api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Api.api.Controllers
{
    public class AreasController : ApiController
    {
        CIREREntities db = new CIREREntities();
        // GET: api/Areas
        public IEnumerable<vw_Areas> Get()
        {
            return db.vw_Areas.OrderBy(o => o.Area);
        }

        /// <summary>
        /// Obtiene las oficinas del área
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Areas/5
        public IHttpActionResult Get(string id)
        {
            //vw_Areas area = db.vw_Areas.FirstOrDefault(w => w.IdArea == id);

            IEnumerable<fn_Oficinas_Result> oficinas = db.fn_Oficinas(id).ToList();
            if (oficinas == null)
            {
                return NotFound();
            }
            return Ok(oficinas);
        }

        // POST: api/Areas
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Areas/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Areas/5
        public void Delete(int id)
        {
        }
    }
}
