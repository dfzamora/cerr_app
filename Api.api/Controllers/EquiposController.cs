﻿using Api.api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.api.Controllers
{
    public class EquiposController : ApiController
    {
        CIREREntities db = new CIREREntities();

        // GET: api/Equipos
        public IEnumerable<vw_ClasificacionProductosSIPPSI> Get()
        {
            return db.vw_ClasificacionProductosSIPPSI.OrderBy(o=>o.Clasificacion);
        }

        /// <summary>
        /// Retorna todos los equipos que pertenecen a la clasificación especificada
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Equipos/5
        public IEnumerable<vw_ProductosSIPPSI_Electricos> Get(string id)
        {
            return db.vw_ProductosSIPPSI_Electricos.Where(w=>w.IdClasificacion==id);
        }

        // POST: api/Equipos
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Equipos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Equipos/5
        public void Delete(int id)
        {
        }
    }
}
