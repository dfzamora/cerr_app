﻿using Api.api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Api.api.Controllers
{
    public class CensoController : ApiController
    {
        CIREREntities db = new CIREREntities();

        // GET: api/Censo
        public IEnumerable<object> Get()
        {
            var censos = db.Censo.Select(s => new
            {
                s.IdCenso,
                s.IdOficina,
                s.CantidadMujeres,
                s.CantidadVarones,
                s.UsuarioCreacion,
                s.FechaCreacion,
            }).ToList();

            return censos;
        }

        /// <summary>
        /// Obtiene el detalle del censo especificado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Censo/5
        public IHttpActionResult Get(int id)
        {
            Censo censo = db.Censo.Find(id);

            if (censo == null)
            {
                return NotFound();
            }

            var detallecensos = censo.DetalleCenso.Select(s => new
            {
                s.IdDetalleCenso,
                s.IdCenso,
                s.IdEquipo,
                s.Cantidad,
                s.Potencia,
                s.Voltaje,
                s.Corriente,
                s.HorasUso,
                s.DiasUso,
                s.FactorPotencia
            });
            return Ok(detallecensos);
        }

        // POST: api/Censo
        public void Post([FromBody]Censo censo, [FromBody]List<DetalleCenso> detalle)
        {

        }

        // PUT: api/Censo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Censo/5
        public void Delete(int id)
        {
        }
    }
}

/*
 
      int agregados = 0, editados = 0, eliminados = 0;

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    AccesoDirectoUsuario au;
                    foreach (AccesoDirectoUsuarioSon item in accesos)
                    {
                        if (item.IdAccesoDirectoUsuario == 0)
                        {
                            if (item.hasAD)
                            {
                                au = new AccesoDirectoUsuario();
                                au.IdPantalla = item.Pantalla.IdPantalla;
                                au.IdUsuario = clsSessionHelper.usuario.Login;
                                au.BackgroundCard = item.BackgroundCard;
                                au.FechaCreacion = System.DateTime.Now;

                                db.AccesoDirectoUsuario.Add(au);
                                agregados++;
                            }
                        }
                        else
                        {
                            au = db.AccesoDirectoUsuario.Find(item.IdAccesoDirectoUsuario);

                            if (!item.hasAD)
                            {
                                db.Entry(au).State = System.Data.Entity.EntityState.Deleted;
                                eliminados++;
                            }
                            else
                            {
                                if (au.BackgroundCard != item.BackgroundCard)
                                {
                                    au.BackgroundCard = item.BackgroundCard;
                                    db.Entry(au).State = System.Data.Entity.EntityState.Modified;
                                    editados++;
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }

            return string.Format("Elementos agregados {0} \nElementos Modificados {1} \nElementos removidos {2}", agregados, editados, eliminados);
        }

     
     */
