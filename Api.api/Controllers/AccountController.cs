﻿using Api.api.Models;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.api.Controllers
{
    public class AccountController : ApiController
    {
        // GET: api/Account
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Account/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Account
        public bool Post([FromBody] AccountModel account)
        {
            var wsOffice = new wsOffice365.authSoapClient();


            return wsOffice.Validate(account.UserName, account.Password);
        }

        // PUT: api/Account/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Account/5
        public void Delete(int id)
        {
        }
    }
}
